var _ = require('lodash');

var CAR_DATA = [
  { manufacturer: 'Chevrolet', models: ['Corvette', 'Camaro', 'Suburban', 'S-10'] },
  { manufacturer: 'Ford', models: ['Escort', 'Focus', 'Mustang', 'Bronco'] },
  { manufacturer: 'Dodge', models: ['Charger', 'Viper', 'Durango'] },
  { manufacturer: 'Fiat', models: ['500', '500c', '500 Abarth'] },
  { manufacturer: 'BMW', models: ['535i Sedan', 'M6 Convertible', 'X5', 'M4 Coupe'] }
];

// Given a CAR_DATA constant that is an array of objects.
// Each object has two properties:
//   manufacturer: A car manufacturer.
//   models: An array of car models.
// 
// Implement the run method to genenerate an array of all possible cars.
// Each array item should be a string in the form "manufacturer: model"
//   Example:
//   [ "Toyota: Camry", "Dodge: Durango", ... ]
//
// You can use plain JS with a utility library if needed.
// lodash is already included.

function test() {
  var fn = (acc, data) =>
    acc.concat(data.models.map(m => {
      return `${data.manufacturer}: ${m}`;
    });

  return CAR_DATA.reduce(fn, []);
}

if (verify(test())) {
  console.log('Verified!');
}

function verify(actual) { 
  var expected = 'WyJDaGV2cm9sZXQ6IENvcnZldHRlIiwiQ2hldnJvbGV0OiBDYW1hcm8iLCJDaGV2cm9sZXQ6IFN1YnVyYmFuIiwiQ2hldnJvbGV0OiBTLTEwIiwiRm9yZDogRXNjb3J0IiwiRm9yZDogRm9jdXMiLCJGb3JkOiBNdXN0YW5nIiwiRm9yZDogQnJvbmNvIiwiRG9kZ2U6IENoYXJnZXIiLCJEb2RnZTogVmlwZXIiLCJEb2RnZTogRHVyYW5nbyIsIkZpYXQ6IDUwMCIsIkZpYXQ6IDUwMGMiLCJGaWF0OiA1MDAgQWJhcnRoIiwiQk1XOiA1MzVpIFNlZGFuIiwiQk1XOiBNNiBDb252ZXJ0aWJsZSIsIkJNVzogWDUiLCJCTVc6IE00IENvdXBlIl0';

  var o = new Buffer(expected, 'base64').toString('utf-8');
  return _.eq(JSON.parse(o), actual);
}

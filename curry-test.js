var _ = require('lodash');

// Please make an add function has the following behavior:
//
// add(x)(y) === (x + y)
// add(4)(5) === 9

function add() {
  // Add code here
}

if (verify()) {
  console.log('Verified!');
} else {
  console.log('Try again.');
}

function verify() { 
  try {
    var x = Math.random()
      , y = Math.random();

    return (add(x)(y) === (x + y));
  } catch (e) {
    return false;
  }
}

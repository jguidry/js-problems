var _ = require('lodash');

// Please make an add function has the following behavior:
//
// add(x)(y) === (x + y)
// add(2)(5) === 7

var add = _.curry(_add);
function _add(x, y) {
  // Add code here
  return x + y;
}

if (verify()) {
  console.log('Verified!');
}

function verify() { 
  try {
    var x = Math.random()
      , y = Math.random();

    return (add(x)(y) === (x + y));
  } catch (e) {
    return false;
  }
}
